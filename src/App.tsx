import React, { Component } from 'react';
import './App.css';
import 'antd/dist/antd.css';
import './normalize.css';
import {
  Route,
  withRouter,
  BrowserRouter,
  Switch,
  Redirect,
  Link,
} from 'react-router-dom';
import Settings from './components/Settings/Settings';
import News from './components/News/News';
import Music from './components/Music/Music';
import { UsersPage } from './components/Users/UsersContainer';
import { Header } from './components/Header/Header';
import { Login } from './components/Login/Login';
import { connect, Provider } from 'react-redux';
import { initializeApp } from './redux/appReducer';
import { compose } from 'redux';
import Preloader from './components/common/preloader/Preloader';
import store, { AppStateType } from './redux/reduxStore';
import { withSuspense } from './hoc/withSuspense';

import { Layout, Menu, Breadcrumb } from 'antd';
import {
  UserOutlined,
  LaptopOutlined,
  NotificationOutlined,
} from '@ant-design/icons';

const { SubMenu } = Menu;
const { Content, Footer, Sider } = Layout;

const DialogsContainer = React.lazy(
  () => import('./components/Dialogs/DialogsContainer')
);
const ProfileContainer = React.lazy(
  () => import('./components/Profile/ProfileContainer')
);
const ChatPage = React.lazy(() => import('./pages/Chat/ChatPage'));

const SuspendedProfile = withSuspense(ProfileContainer);
const SuspendedDialogs = withSuspense(DialogsContainer);
const SuspendedChatPage = withSuspense(ChatPage);

type MapPropsType = ReturnType<typeof mapStateToProps>;
type DispatchPropsType = {
  initializeApp: () => void;
};

class App extends Component<MapPropsType & DispatchPropsType> {
  handleAllError = (promiseRejectionEvent: PromiseRejectionEvent) =>
    alert('Some error occurred');
  componentDidMount() {
    this.props.initializeApp();
    window.addEventListener('unhandledrejection', this.handleAllError);
  }

  componentWillUnmount() {
    window.removeEventListener('unhandledrejection', this.handleAllError);
  }

  render() {
    if (this.props.initialized) return <Preloader />;
    return (
      <Layout>
        <Header />
        <Content style={{ padding: '0 50px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <Layout
            className="site-layout-background"
            style={{ padding: '24px 0' }}
          >
            <Sider className="site-layout-background" width={200}>
              <Menu
                mode="inline"
                // defaultSelectedKeys={['1']}
                // defaultOpenKeys={['sub1']}
                style={{ height: '100%' }}
              >
                <SubMenu key="sub1" icon={<UserOutlined />} title="My Profile">
                  <Menu.Item key="1">
                    <Link to="/profile">Profile</Link>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <Link to="/dialogs">Dialogs</Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub2"
                  icon={<LaptopOutlined />}
                  title="Developers"
                >
                  <Menu.Item key="5">
                    <Link to="/developers">Developers</Link>
                  </Menu.Item>
                </SubMenu>
                <SubMenu
                  key="sub3"
                  icon={<NotificationOutlined />}
                  title="Other"
                >
                  <Menu.Item key="9">
                    <Link to="/chat">Chat</Link>
                  </Menu.Item>
                  <Menu.Item key="10">
                    <Link to="/news">News</Link>
                  </Menu.Item>
                  <Menu.Item key="11">
                    <Link to="/music">Music</Link>
                  </Menu.Item>
                  <Menu.Item key="12">
                    <Link to="/settings">Settings</Link>
                  </Menu.Item>
                </SubMenu>
              </Menu>
            </Sider>
            <Content style={{ padding: '0 24px', minHeight: 280 }}>
              <Switch>
                <Route
                  exact
                  path="/"
                  render={() => <Redirect to={'/profile'} />}
                />
                <Route
                  path="/profile/:userId?"
                  render={() => <SuspendedProfile />}
                />
                <Route path="/dialogs" render={() => <SuspendedDialogs />} />
                <Route path="/music" render={() => <Music />} />
                <Route path="/news" render={() => <News />} />
                <Route path="/settings" render={() => <Settings />} />
                <Route
                  path="/developers"
                  render={() => <UsersPage pageTitle={'Самурай'} />}
                />
                <Route path="/login" render={() => <Login />} />
                <Route path="/chat" render={() => <SuspendedChatPage />} />
                <Route path="*" render={() => <div>NOT FOUND</div>} />
              </Switch>
            </Content>
          </Layout>
        </Content>
        <Footer style={{ textAlign: 'center' }}>
          Social Network ©2021 Created by Vadymk95@gmail.com
        </Footer>
      </Layout>

      // <div className="container">
      //   <HeaderContainer />
      //   <Navbar />
      //   <div className="app-wrapper-content">
      //     <Switch>
      //       <Route exact path="/" render={() => <Redirect to={'/profile'} />} />
      //       <Route
      //         path="/profile/:userId?"
      //         render={() => <SuspendedProfile />}
      //       />
      //       <Route path="/dialogs" render={() => <SuspendedDialogs />} />
      //       <Route path="/music" render={() => <Music />} />
      //       <Route path="/news" render={() => <News />} />
      //       <Route path="/settings" render={() => <Settings />} />
      //       <Route
      //         path="/users"
      //         render={() => <UsersPage pageTitle={'Самурай'} />}
      //       />
      //       <Route path="/login" render={() => <Login />} />
      //       <Route path="*" render={() => <div>NOT FOUND</div>} />
      //     </Switch>
      //   </div>
      // </div>
    );
  }
}

const mapStateToProps = (state: AppStateType) => ({
  initialized: state.app.initialized,
});

const AppContainer = compose<React.ComponentType>(
  withRouter,
  connect(mapStateToProps, { initializeApp })
)(App);

const MyApp: React.FC = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <AppContainer />
      </Provider>
    </BrowserRouter>
  );
};

export default MyApp;
