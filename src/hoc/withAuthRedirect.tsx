import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { AppStateType } from '../redux/reduxStore';

let mapStateToPropsForRedirect = (state: AppStateType) => ({
  isAuth: state.auth.isAuth,
});

type MapPropsType = {
  isAuth: boolean;
};

type DispatchPropsType = {

}

export function withAuthRedirect<WCP>(
  WrappedComponent: React.ComponentType<WCP>
) {
  function RedirectComponent(props: DispatchPropsType & MapPropsType) {
    let { isAuth, ...restProps } = props;
    if (!props.isAuth) return <Redirect to="/login" />;
    return <WrappedComponent {...restProps as unknown as WCP} />;
  }

  let ConnectedAuthRedirectComponent = connect<
    MapPropsType,
    DispatchPropsType,
    WCP,
    AppStateType
  >(mapStateToPropsForRedirect, {})(RedirectComponent);

  return ConnectedAuthRedirectComponent;
}
