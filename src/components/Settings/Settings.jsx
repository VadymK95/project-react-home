import React from 'react';
import SettingsStyles from './Settings.module.css';

const Settings = () => {
  return (
    <div className={SettingsStyles.settings}>
      <h1 className={SettingsStyles.title}>Settings</h1>
    </div>
  );
};

export default Settings;
