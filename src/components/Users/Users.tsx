import React, { useEffect } from 'react';
import UsersStyles from './Users.module.css';
import Paginator from '../common/Paginator/Paginator';
import User from './User';
import { UsersSearchForm } from './UsersSearchForm';
import { FilterType, getUsers } from '../../redux/usersReducer';
import {
  getCurrentPage,
  getFollowingInProgress,
  getPageSize,
  getTotalUsersCount,
  getUser,
  getUsersFilter,
} from '../../redux/usersSelectors';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import queryString from 'querystring';

type PropsType = {
  portionSize?: number;
};

export const Users: React.FC<PropsType> = props => {
  const totalUsersCount = useSelector(getTotalUsersCount);
  const users = useSelector(getUser);
  const currentPage = useSelector(getCurrentPage);
  const pageSize = useSelector(getPageSize);
  const filter = useSelector(getUsersFilter);
  const followingInProgress = useSelector(getFollowingInProgress);

  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const parsed = queryString.parse(history.location.search.substr(1)) as {
      term: string;
      page: string;
      friend: string;
    };

    let actualPage = currentPage;
    let actualFilter = filter;

    if (!!parsed.page) actualPage = Number(parsed.page);
    if (!!parsed.term)
      actualFilter = { ...actualFilter, term: parsed.term as string };
    switch (parsed.friend) {
      case 'null':
        actualFilter = { ...actualFilter, friend: null };
        break;
      case 'true':
        actualFilter = { ...actualFilter, friend: true };
        break;
      case 'false':
        actualFilter = { ...actualFilter, friend: false };
        break;
    }

    dispatch(getUsers(actualPage, pageSize, actualFilter));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); //maybe empty

  useEffect(() => {
    const query: {
      term?: string;
      page?: string;
      friend?: string;
    } = {};

    if (!!filter.term) query.term = filter.term;
    if (filter.friend !== null) query.friend = String(filter.friend);
    if (currentPage !== 1) query.page = String(currentPage);

    history.push({
      pathname: '/developers',
      search: queryString.stringify(query),
    });
  }, [currentPage, filter, history]);

  const onPageChanged = (pageNumber: number) => {
    dispatch(getUsers(pageNumber, pageSize, filter));
  };

  const onFilterChanged = (filter: FilterType) => {
    dispatch(getUsers(1, pageSize, filter));
  };

  const follow = (userId: number) => {
    dispatch(follow(userId));
  };
  const unfollow = (userId: number) => {
    dispatch(unfollow(userId));
  };

  return (
    <div className={UsersStyles.users}>
      <h1 className={UsersStyles.title}>Users</h1>

      <div>
        <UsersSearchForm onFilterChanged={onFilterChanged} />
      </div>

      <Paginator
        currentPage={currentPage}
        onPageChanged={onPageChanged}
        totalUsersCount={totalUsersCount}
        pageSize={pageSize}
        portionSize={props.portionSize}
      />
      {users.map((u: any) => (
        <User
          user={u}
          followingInProgress={followingInProgress}
          key={u.id}
          follow={follow}
          unfollow={unfollow}
        />
      ))}
    </div>
  );
};
