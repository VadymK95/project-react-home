import React from 'react';
import { NavLink } from 'react-router-dom';
import { UserType } from '../../types/types';
import UserPhoto from './../assets/images/avatar.jpg';
import UsersStyles from './Users.module.css';

type PropsType = {
  user: UserType;
  followingInProgress: Array<number>;
  unfollow: (userId: number) => void;
  follow: (userId: number) => void;
}

const User: React.FC<PropsType> = ({ user, followingInProgress, unfollow, follow }) => {
  return (
    <div className={UsersStyles.user} key={user.id}>
      <div className={UsersStyles.userAction}>
        <div className={UsersStyles.userAvatar}>
          <NavLink to={'/profile/' + user.id}>
            <img
              src={user.photos.small !== null ? user.photos.small : UserPhoto}
              alt="avatar"
            />
          </NavLink>
        </div>
        <div className={UsersStyles.userButton}>
          {user.followed ? (
            <button
              disabled={followingInProgress.some(id => id === user.id)}
              onClick={() => {
                unfollow(user.id);
              }}
              className={UsersStyles.userBtn}
            >
              unfollow
            </button>
          ) : (
            <button
              disabled={followingInProgress.some(id => id === user.id)}
              onClick={() => {
                follow(user.id);
              }}
              className={UsersStyles.userBtn}
            >
              follow
            </button>
          )}
        </div>
      </div>
      <div className={UsersStyles.userInfo}>
        <div className={UsersStyles.userName}>{user.name}</div>
        <div className={UsersStyles.userCountry}>{'u.location.country'}</div>
        <div className={UsersStyles.userStatus}>{user.status}</div>
        <div className={UsersStyles.userCity}>{'u.location.city'}</div>
      </div>
    </div>
  );
};

export default User;
