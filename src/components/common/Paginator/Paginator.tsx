import React, { useState } from 'react';
import PaginatorStyles from './Paginator.module.css';
import cn from 'classnames';

type Props = {
  currentPage: number;
  onPageChanged: (pageNumber: number) => void;
  totalUsersCount: number;
  pageSize: number;
  portionSize?: number;
};

let Paginator: React.FC<Props> = ({
  currentPage,
  onPageChanged,
  totalUsersCount,
  pageSize,
  portionSize = 10,
}) => {
  let pagesCount = Math.ceil(totalUsersCount / pageSize);

  let pages: Array<number> = [];
  for (let i = 1; i <= pagesCount; i++) pages.push(i);

  let portionCount = Math.ceil(pagesCount / portionSize);
  let [portionNumber, setPortionNumber] = useState(1);
  let leftPortionPageNumber = (portionNumber - 1) * portionSize + 1;
  let rightPortionPageNumber = portionNumber * portionSize;

  return (
    <div className={cn(PaginatorStyles.pagination)}>
      {portionNumber > 1 && (
        <button
          onClick={() => {
            setPortionNumber(portionNumber - 1);
          }}
        >
          PREV
        </button>
      )}

      {pages
        .filter(p => p >= leftPortionPageNumber && p <= rightPortionPageNumber)
        .map(p => {
          return (
            <span
              className={cn(
                {
                  [PaginatorStyles.selected]: currentPage === p,
                },
                PaginatorStyles.page
              )}
              key={p}
              onClick={e => {
                onPageChanged(p);
              }}
            >
              {p}
            </span>
          );
        })}

      {portionCount > portionNumber && (
        <button
          onClick={() => {
            setPortionNumber(portionNumber + 1);
          }}
        >
          NEXT
        </button>
      )}
    </div>
  );
};

export default Paginator;
