import React from 'react';
import PreloaderStyles from './Preloader.module.css';
import preloader from './../../assets/images/spinner.gif';

const Preloader: React.FC= () => {
  return (
    <div className={PreloaderStyles.preloader}>
      <img className={PreloaderStyles.imgLoad} src={preloader} alt="" />
    </div>
  );
};

export default Preloader;
