import React from 'react';
import { Field, WrappedFieldProps } from 'redux-form';
import { FieldValidatorType } from '../../../utils/validators/validators';
import { LoginFormValuesType } from '../../Login/Login';
import FormsControlsStyles from './FormsControls.module.css';

type ElementType = (Element: React.FC | string) => React.FC<WrappedFieldProps>;

const Element: ElementType =
  Element =>
  ({ input, meta: { touched, error }, ...props }) => {
    const hasError = touched && error;
    return (
      <div
        className={
          FormsControlsStyles.formControl +
          ' ' +
          (hasError ? FormsControlsStyles.error : '')
        }
      >
        <Element {...input} {...props} />
        {hasError && <span> {error} </span>}
      </div>
    );
  };
export default Element;

export type LoginFormValuesTypeKey = GetStringKeys<LoginFormValuesType>;

export function createField<FormKeysType extends string>(
  placeholder: string | undefined,
  name: FormKeysType,
  type: string | null,
  validators: Array<FieldValidatorType>,
  component: React.FC<WrappedFieldProps>,
  style: any
) {
  return (
    <Field
      className={style}
      validate={validators}
      name={name}
      component={component}
      type={type}
      placeholder={placeholder}
    />
  );
}

export type GetStringKeys<T> = Extract<keyof T, string>;
