import React from 'react';
import { NavLink } from 'react-router-dom';
import LinkStyles from './Link.module.css';

const Link = ({ href, name }) => {
  return (
    <div>
      <NavLink
        className={`${LinkStyles.link}`}
        activeClassName={LinkStyles.active}
        to={href}
      >
        {name}
      </NavLink>
    </div>
  );
};

export default Link;
