import React from 'react';
import NavStyles from './Navbar.module.css';
import { NavLink } from 'react-router-dom';

const Navbar: React.FC = props => {
  return (
    <div className={NavStyles.navbar}>
      <nav className={NavStyles.nav}>
        <NavLink activeClassName="active" to="/profile">
          Profile
        </NavLink>
        <NavLink activeClassName="active" to="/developers">
          Developers
        </NavLink>
        <NavLink activeClassName="active" to="/dialogs">
          Dialogs
        </NavLink>
        <NavLink activeClassName="active" to="/news">
          News
        </NavLink>
        <NavLink activeClassName="active" to="/music">
          Music
        </NavLink>
        <NavLink activeClassName="active" to="/settings">
          Settings
        </NavLink>
      </nav>
      <div className={NavStyles.friends}>
        <h2 className={NavStyles.friendsTitle}>Friends</h2>
        <ul className={NavStyles.friendsList}>{/* { friendElements } */}</ul>
      </div>
    </div>
  );
};

export default Navbar;
