import React from 'react';
import FriendStyle from './Friend.module.css';

const Friend = props => {
  return (
    <li className={FriendStyle.friend}>
      <a className={FriendStyle.friendLink} href="#">
        <img
          src={props.photo}
          alt="friend"
          className={FriendStyle.friendPhoto}
        />
      </a>
      <p className={FriendStyle.friendName}> {props.name} </p>
    </li>
  );
};

export default Friend;
