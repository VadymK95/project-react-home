import {
  instance,
  LoginResponseDataType,
  MeResponseType,
  ResponseType,
  ResultCodesEnum,
  ResultCodesWithCaptchaEnum,
} from './api';

export const authAPI = {
  me() {
    return instance
      .get<ResponseType<MeResponseType>>('auth/me')
      .then(response => response.data);
  },
  login(
    email: string,
    password: string,
    rememberMe = false,
    captcha: null | string = null
  ) {
    return instance
      .post<
        ResponseType<
          LoginResponseDataType,
          ResultCodesEnum | ResultCodesWithCaptchaEnum
        >
      >('auth/login', {
        email,
        password,
        rememberMe,
        captcha,
      })
      .then(response => response.data);
  },
  logout() {
    return instance.delete('auth/login').then(response => response.data);
  },
};
