import { GetItemsType, instance, ResponseType } from './api';
import { profileAPI } from './profileAPI';

export const usersAPI = {
  getUsers(pageNumber = 1, pageSize = 10, term: string = '', friend: null | boolean = null) {
    return instance
      .get<GetItemsType>(`users?page=${pageNumber}&count=${pageSize}&term=${term}` + (friend === null ? '' : `&friend=${friend}`))
      .then(res => res.data);
  },
  follow(userId: number) {
    return instance
      .post<ResponseType>(`follow/${userId}`)
      .then(res => res.data);
  },
  unfollow(userId: number) {
    return instance
      .delete<ResponseType>(`follow/${userId}`)
      .then(res => res.data);
  },
  getProfile(userId: number) {
    return profileAPI.getProfile(userId);
  },
};
