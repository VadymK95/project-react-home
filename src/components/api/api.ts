import axios from 'axios';
import { UserType } from '../../types/types';

export const instance = axios.create({
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  withCredentials: true,
  headers: {
    'API-KEY': '1773f4d3-a3fd-4ac5-b463-c676d92148e7',
  },
});

export type ResponseType<D = {}, RC = ResultCodesEnum> = {
  data: D;
  messages: Array<string>;
  resultCode: RC;
};

export enum ResultCodesEnum {
  Success = 0,
  Error = 1,
}

export enum ResultCodesWithCaptchaEnum {
  CaptchaIsValid = 10,
}

export type MeResponseType = {
  id: number;
  email: string;
  login: string;
};

export type LoginResponseDataType = {
  userId: number;
};

export type GetItemsType = {
  items: Array<UserType>;
  totalCount: number;
  error: string | null;
};
