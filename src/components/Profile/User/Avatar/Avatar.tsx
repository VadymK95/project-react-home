import React, { ChangeEvent, useState } from 'react';
import Preloader from '../../../common/preloader/Preloader';
import AvatarStyles from './Avatar.module.css';
import UserPhoto from './../../../assets/images/avatar.jpg';
import UserBlog from '../UserBlog/UserBlog';
import UserBlogFormReduxForm from '../UserBlogForm/UserBlogForm';
import { ProfileType } from '../../../../types/types';

export type PropsType = {
  profile: ProfileType | null;
  status?: string;
  updateStatus?: (status: string) => void;
  isOwner: boolean;
  savePhoto: (file: File) => void;
  saveProfile: (profile: ProfileType) => Promise<any>;
}

const Avatar: React.FC<PropsType>= props => {
  let [editMode, setEditMode] = useState(false);

  if (!props.profile) {
    return <Preloader />;
  }

  const onMainPhotoSelected = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.files?.length) {
      props.savePhoto(e.target.files[0]);
    }
  };

  const onSubmit = (formData: ProfileType) => {
    props.saveProfile(formData).then(() => setEditMode(false));
  };

  return (
    <div className={AvatarStyles.userPhoto}>
      <img
        src={props.profile.photos.large || UserPhoto}
        className={AvatarStyles.mainAvatar}
        alt=""
      />
      {props.isOwner && (
        <input
          className={AvatarStyles.fileInput}
          type="file"
          onChange={onMainPhotoSelected}
        />
      )}
      {editMode ? (
        <UserBlogFormReduxForm
          initialValues={props.profile}
          onSubmit={onSubmit}
          profile={props.profile}
        />
      ) : (
        <UserBlog
          goToEditMode={() => setEditMode(true)}
          profile={props.profile}
          isOwner={props.isOwner}
        />
      )}
    </div>
  );
};

export default Avatar;
