import React from 'react';
import { ContactsType, ProfileType } from '../../../../types/types';
import Contact from '../Contact/Contact';
import UserBlogStyles from './UserBlog.module.css';

type ProfileDataPropsType = {
  isOwner: boolean;
  profile: ProfileType;
  goToEditMode: () => void;
};

const UserBlog: React.FC<ProfileDataPropsType> = props => {
  return (
    <div className={UserBlogStyles.description}>
      {props.isOwner && (
        <div>
          <button onClick={() => props.goToEditMode()}>edit</button>
        </div>
      )}
      <div className={UserBlogStyles.description__item}>
        <b>Nickname</b>: {props.profile.fullName}
      </div>
      <div className={UserBlogStyles.description__item}>
        <b>Looking for a job</b>: {props.profile.lookingForAJob ? 'yes' : 'no'}
      </div>
      {props.profile.lookingForAJob && (
        <div className={UserBlogStyles.description__item}>
          <b>My proffesional skilles</b>:{' '}
          {props.profile.lookingForAJobDescription}
        </div>
      )}
      <div className={UserBlogStyles.description__item}>
        <b>About me</b>: {props.profile.aboutMe}
      </div>
      <div className={UserBlogStyles.description__item}>
        <b>Contacts</b>:{' '}
        {Object.keys(props.profile.contacts).map(key => {
          return (
            <Contact
              key={key}
              contactTitle={key}
              contactValue={props.profile.contacts[key as keyof ContactsType]}
            />
          );
        })}
      </div>
    </div>
  );
};

export default UserBlog;
