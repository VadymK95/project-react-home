import React, { ChangeEvent } from 'react';
import DescriptionStyles from './Description.module.css';

type PropsType = {
  status: string;
  updateStatus: (newStatus: boolean) => void;
};

type StateType = {
  editMode: boolean;
  status: string;
};

class Description extends React.Component<PropsType, StateType> {
  state: any = {
    editMode: false,
    status: !this.props.status,
  };

  activateEditMode = () => {
    this.setState({
      editMode: true,
    });
  };

  deactivateEditMode = () => {
    this.setState({
      editMode: false,
    });
    this.props.updateStatus(this.state.status);
  };

  onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
    this.setState({
      status: e.currentTarget.value,
    });
  };

  componentDidUpdate(prevProps: PropsType, prevState: StateType) {
    if (prevProps.status !== this.props.status) {
      this.setState({ status: this.props.status });
    }
  }

  render() {
    return (
      <div className={DescriptionStyles.description}>
        {!this.state.editMode && (
          <div>
            <span
              onDoubleClick={this.activateEditMode}
              className={DescriptionStyles.status}
            >
              {this.props.status || 'NO STATUS'}
            </span>
          </div>
        )}
        {this.state.editMode && (
          <div>
            <input
              onChange={this.onStatusChange}
              autoFocus={true}
              onBlur={this.deactivateEditMode}
              value={this.props.status}
              className={DescriptionStyles.statusInput}
              type="text"
            />
          </div>
        )}
      </div>
    );
  }
}

export default Description;
