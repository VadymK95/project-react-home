import React, { useState, useEffect, ChangeEvent } from 'react';
import DescriptionStyles from './Description.module.css';

type PropsType = {
  status: string;
  updateStatus: (status: string) => void;
  value?: string;
};

const DescriptionWithHooks: React.FC<PropsType> = props => {
  const [state, setstate] = useState(false);
  const [status, setstatus] = useState(props.status);

  useEffect(() => {
    setstatus(props.status);
  }, [props.status]);

  const activateMode = () => {
    setstate(true);
  };

  const deactivateMode = () => {
    setstate(false);
    props.updateStatus(status);
  };

  const onStatusChange = (e: ChangeEvent<HTMLInputElement>) => {
    setstatus(e.currentTarget.value);
  };

  return (
    <div className={DescriptionStyles.description}>
      {!state && (
        <div>
          <span
            onDoubleClick={activateMode}
            className={DescriptionStyles.status}
          >
            {props.status || 'NO STATUS'}
          </span>
        </div>
      )}
      {state && (
        <div>
          <input
            autoFocus={true}
            onChange={onStatusChange}
            className={DescriptionStyles.statusInput}
            onBlur={deactivateMode}
            value={status}
            type="text"
          />
        </div>
      )}
    </div>
  );
};

export default DescriptionWithHooks;
