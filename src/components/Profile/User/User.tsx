import React from 'react';
import UserStyles from './User.module.css';
import Avatar from './Avatar/Avatar';
import DescriptionWithHooks from './Description/DescriptionWithHooks';
import { PropsType } from '../Profile';


const User: React.FC<PropsType> = props => {
  return (
    <div className={UserStyles.user}>
      <Avatar
        isOwner={props.isOwner}
        profile={props.profile}
        savePhoto={props.savePhoto}
        saveProfile={props.saveProfile}
      />
      <DescriptionWithHooks
        status={props.status}
        updateStatus={props.updateStatus}
      />
    </div>
  );
};

export default User;
