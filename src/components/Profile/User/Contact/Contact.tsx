import React from 'react';
import ContactStyles from './Contact.module.css';

type ContactsPropsType = {
  contactTitle: string;
  contactValue: string;
}

const Contact: React.FC<ContactsPropsType> = ({ contactTitle, contactValue }) => {
  return (
    <p className={ContactStyles.contact__item}>
      <b>{contactTitle}</b>: {contactValue}
    </p>
  );
};

export default Contact;
