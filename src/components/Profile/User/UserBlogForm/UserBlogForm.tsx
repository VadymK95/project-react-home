import React from 'react';
import { InjectedFormProps, reduxForm } from 'redux-form';
import { ProfileType } from '../../../../types/types';
import Element, {
  createField,
  GetStringKeys,
} from '../../../common/FormsControls/FormsControls';
import UserBlogFormStyles from './UserBlogForm.module.css';

const Input = Element('input');
const TextArea = Element('textarea');

type PropsType = {
  profile: ProfileType;
};
type ProfileTypeKeys = GetStringKeys<ProfileType>;

const UserBlogForm: React.FC<
  InjectedFormProps<ProfileType, PropsType> & PropsType
> = props => {
  return (
    <form
      onSubmit={props.handleSubmit}
      className={UserBlogFormStyles.description}
    >
      {
        <div>
          <button onClick={() => {}}>save</button>
          {props.error && (
            <p className={UserBlogFormStyles.error}>{props.error}</p>
          )}
        </div>
      }
      <div className={UserBlogFormStyles.description__item}>
        <b>Nickname</b>:{' '}
        {createField<ProfileTypeKeys>(
          'Nickname',
          'fullName',
          'text',
          [],
          Input,
          UserBlogFormStyles.input
        )}
      </div>
      <div className={UserBlogFormStyles.description__item}>
        <b>Looking for a job</b>:{' '}
        {createField<ProfileTypeKeys>(
          undefined,
          'lookingForAJob',
          'checkbox',
          [],
          Input,
          UserBlogFormStyles.input
        )}
      </div>
      <div className={UserBlogFormStyles.description__item}>
        <b>My proffesional skilles</b>:{' '}
        {createField<ProfileTypeKeys>(
          'My skilles',
          'lookingForAJobDescription',
          'text',
          [],
          TextArea,
          UserBlogFormStyles.input
        )}
      </div>
      <div className={UserBlogFormStyles.description__item}>
        <b>About me</b>:{' '}
        {createField<ProfileTypeKeys>(
          'About me',
          'aboutMe',
          'text',
          [],
          TextArea,
          UserBlogFormStyles.input
        )}
      </div>
      <div className={UserBlogFormStyles.description__item}>
        <b>Contacts</b>:{' '}
        {Object.keys(props.profile.contacts).map(key => {
          return (
            <div key={key} className={UserBlogFormStyles.contacts}>
              <b>
                {key}:{' '}
                {createField(
                  key,
                  'contacts.' + key,
                  'text',
                  [],
                  Input,
                  UserBlogFormStyles.input
                )}{' '}
              </b>
            </div>
          );
        })}
      </div>
    </form>
  );
};

const UserBlogFormReduxForm = reduxForm<ProfileType, PropsType>({
  form: 'edit-profile',
})(UserBlogForm);
export default UserBlogFormReduxForm;
