import React from 'react';
import MainPhotoStyles from './MainPhoto.module.css';

const MainPhoto = () => {
  return (
    <div className={MainPhotoStyles.mainPhoto}>
      <img src="https://wallpapercave.com/wp/wp4100961.jpg" alt="" />
    </div>
  );
};

export default MainPhoto;
