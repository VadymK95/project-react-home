import React from 'react';
import PostStyles from './Post.module.css';

type PropsType = {
  text: string;
  likesCount: number;
};

const Post: React.FC<PropsType> = ({ text, likesCount }) => {
  return (
    <div className={PostStyles.post}>
      <div className={PostStyles.postPhoto}>
        <img
          src="https://st.depositphotos.com/1007995/1274/i/600/depositphotos_12747026-stock-photo-black-and-white-cat-wearing.jpg"
          alt=""
        />
      </div>
      <div className={PostStyles.postText}>
        <p>{text}</p>
      </div>
      <div className={PostStyles.btnLike}>
        <span>Likes: {likesCount}</span>
      </div>
    </div>
  );
};

export default Post;
