import React from 'react';
import MyPostsStyles from './MyPosts.module.css';
import Post from './Post/Post';
import TextForm, { AddPostFormValuesType } from './TextForm/TextForm';
import { PostsType } from '../../../types/types';

export type MapPropsType = {
  posts: Array<PostsType>;
};

export type DispatchPropsType = {
  addPost: (newPostText: string) => void;
};

const MyPosts: React.FC<MapPropsType & DispatchPropsType> = props => {
  const onAddPost = (values: AddPostFormValuesType) => {
    props.addPost(values.newPostText);
  };

  let postElements = [...props.posts].map((p, key) => (
    <Post text={p.textPost} key={key} likesCount={p.likes} />
  ));
  return (
    <div className={MyPostsStyles.posts}>
      <h2 className={MyPostsStyles.titlePost}>My posts</h2>
      <TextForm onSubmit={onAddPost} />
      <div className={MyPostsStyles.newPost}>{postElements}</div>
    </div>
  );
};

React.memo(MyPosts);

export default MyPosts;
