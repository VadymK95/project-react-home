import React from 'react';
import TextFormStyles from './TextForm.module.css';
import { InjectedFormProps, reduxForm } from 'redux-form';
import {
  maxLengthCreator,
  required,
} from '../../../../utils/validators/validators';
import Element, {
  createField,
  GetStringKeys,
} from '../../../common/FormsControls/FormsControls';

const maxLength10 = maxLengthCreator(10);
const Textarea = Element('textarea');

type PropsType = {};

export type AddPostFormValuesType = {
  newPostText: string;
};

export type AddPostFormValuesTypeKey = GetStringKeys<AddPostFormValuesType>;

const TextForm: React.FC<
  InjectedFormProps<AddPostFormValuesType, PropsType> & PropsType
> = props => {
  return (
    <form onSubmit={props.handleSubmit} className={TextFormStyles.inputPost}>
      {createField<AddPostFormValuesTypeKey>(
        'Enter your text',
        'newPostText',
        'text',
        [required, maxLength10],
        Textarea,
        TextFormStyles.textarea
      )}
      <button className={TextFormStyles.textareaButton}>Send</button>
    </form>
  );
};
export default reduxForm<AddPostFormValuesType, PropsType>({
  form: 'profileAddPostForm',
})(TextForm);
