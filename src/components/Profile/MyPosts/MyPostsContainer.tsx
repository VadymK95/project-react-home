import { connect } from 'react-redux';
import MyPosts, { DispatchPropsType, MapPropsType } from './MyPosts';
import { actions } from '../../../redux/profileReducer';
import { AppStateType } from '../../../redux/reduxStore';

let mapStateToProps = (state: AppStateType) => {
  return {
    state: state,
    posts: state.profilePage.posts,
  };
};

const MyPostsContainer = connect<
  MapPropsType,
  DispatchPropsType,
  {},
  AppStateType
>(mapStateToProps, {
  addPost: actions.addPostActionCreator,
})(MyPosts);

export default MyPostsContainer;
