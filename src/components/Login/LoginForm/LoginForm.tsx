import React from 'react';
import { InjectedFormProps } from 'redux-form';
import { required } from '../../../utils/validators/validators';
import Element, {
  createField,
  LoginFormValuesTypeKey,
} from '../../common/FormsControls/FormsControls';
import { LoginFormValuesType } from '../Login';
import LoginFormStyles from './LoginForm.module.css';

const Input: any = Element('input');

export type LoginFormOwnProps = {
  captchaUrl: string | null;
};

const LoginForm: React.FC<
  InjectedFormProps<LoginFormValuesType, LoginFormOwnProps> & LoginFormOwnProps
> = ({ handleSubmit, error, captchaUrl }) => {
  return (
    <form onSubmit={handleSubmit} className={LoginFormStyles.loginForm}>
      {createField<LoginFormValuesTypeKey>(
        'Email',
        'email',
        'text',
        [required],
        Input,
        LoginFormStyles.input
      )}
      {createField<LoginFormValuesTypeKey>(
        'Password',
        'password',
        'password',
        [required],
        Input,
        LoginFormStyles.input
      )}
      {createField<LoginFormValuesTypeKey>(
        undefined,
        'rememberMe',
        'checkbox',
        [],
        Input,
        LoginFormStyles.checkbox
      )}

      {captchaUrl && <img src={captchaUrl} alt="" />}
      {captchaUrl &&
        createField<LoginFormValuesTypeKey>(
          'symbols from image',
          'captcha',
          'text',
          [required],
          Input,
          LoginFormStyles.captcha
        )}
      <span>Remember me</span>
      {error && <p className={LoginFormStyles.error}>{error}</p>}
      <div>
        <button className={LoginFormStyles.loginFormBtn}>Login</button>
      </div>
    </form>
  );
};

export default LoginForm;
