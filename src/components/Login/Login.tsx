import React from 'react';
import LoginStyles from './Login.module.css';
import LoginForm, { LoginFormOwnProps } from './LoginForm/LoginForm';
import { reduxForm } from 'redux-form';
import { useDispatch, useSelector } from 'react-redux';
import { login } from './../../redux/authReducer';
import { Redirect } from 'react-router';
import { AppStateType } from '../../redux/reduxStore';

const LoginReduxForm = reduxForm<LoginFormValuesType, LoginFormOwnProps>({
  form: 'login',
})(LoginForm);

export type LoginFormValuesType = {
  email: string;
  password: string;
  rememberMe: boolean;
  captcha: string;
};

export const Login: React.FC = props => {
  const captchaUrl = useSelector(
    (state: AppStateType) => state.auth.captchaUrl
  );
  const isAuth = useSelector((state: AppStateType) => state.auth.isAuth);

  const dispatch = useDispatch();

  const onSubmit = (formData: any) => {
    dispatch(
      login(
        formData.email,
        formData.password,
        formData.rememberMe,
        formData.captcha
      )
    );
  };

  if (isAuth) return <Redirect to={'/profile'} />;
  return (
    <div className={LoginStyles.login}>
      <h1 className={LoginStyles.title}>Login</h1>
      <LoginReduxForm onSubmit={onSubmit} captchaUrl={captchaUrl} />
    </div>
  );
};
