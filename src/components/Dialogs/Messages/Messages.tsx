import React from 'react';
import MessagesStyles from './Messages.module.css';

type PropsType = {
  text: string;
};

const Messages: React.FC<PropsType> = props => {
  return <div className={MessagesStyles.message}>{props.text}</div>;
};

export default Messages;
