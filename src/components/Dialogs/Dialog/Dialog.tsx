import React from 'react';
import { NavLink } from 'react-router-dom';
import DialogStyles from './Dialog.module.css';

type PropsType = {
  id: number;
  name: string;
};

const Dialog: React.FC<PropsType> = props => {
  let path = '/dialogs/' + props.id;

  return (
    <div className={DialogStyles.dialog}>
      <NavLink to={path}>{props.name}</NavLink>
    </div>
  );
};

export default Dialog;
