import React from 'react';
import DialogFormsStyles from './DialogForm.module.css';
import Element, { createField } from '../../common/FormsControls/FormsControls';
import {
  maxLengthCreator,
  required,
} from '../../../utils/validators/validators';
import { NewMsgFormType } from '../Dialogs';
import { InjectedFormProps } from 'redux-form';

const maxLength50 = maxLengthCreator(50);
const Textarea = Element('textarea');

type NewMsgFormValuesKeysType = Extract<keyof NewMsgFormType, string>;
type PropsType = {};

const DialogForm: React.FC<InjectedFormProps<NewMsgFormType> & PropsType> =
  props => {
    return (
      <form
        onSubmit={props.handleSubmit}
        className={DialogFormsStyles.textareaBlock}
      >
        {createField<NewMsgFormValuesKeysType>(
          'Enter your message',
          'newMsgText',
          'text',
          [required, maxLength50],
          Textarea,
          DialogFormsStyles.textarea
        )}
        <button className={DialogFormsStyles.textareaButton}>Send</button>
      </form>
    );
  };

export default DialogForm;
