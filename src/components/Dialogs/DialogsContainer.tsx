import { connect } from 'react-redux';
import { withAuthRedirect } from '../../hoc/withAuthRedirect';
import { actions } from '../../redux/dialogsReducer';
import Dialogs from './Dialogs';
import { compose } from 'redux';
import { AppStateType } from '../../redux/reduxStore';

let mapStateToProps = (state: AppStateType | any) => {
  return {
    dialogsElem: state.dialogsPage.dialogs,
    newMsgText: state.dialogsPage.newMsgText,
    messagesElem: state.dialogsPage.messages,
  };
};

export default compose<React.ComponentType>(
  connect(mapStateToProps, {
    ...actions,
  }),
  withAuthRedirect
)(Dialogs);
