import React from 'react';
import DialogsStyles from './Dialogs.module.css';
import Dialog from './Dialog/Dialog';
import Messages from './Messages/Messages';
import DialogForm from './DialogForm/DialogForm';
import { reduxForm } from 'redux-form';
import { InitialStateType } from '../../redux/dialogsReducer';

export type NewMsgFormType = {
  newMsgText: string;
};

type PropsType = {
  dialogsElem: InitialStateType;
  messagesElem: InitialStateType;
  addMsg: (msgText: string) => void;
};

const Dialogs: React.FC<PropsType> = props => {
  const MessageReduxForm = reduxForm<NewMsgFormType>({
    form: 'dialogAddMessageForm',
  })(DialogForm);

  const addNewMessage = (values: NewMsgFormType) => {
    props.addMsg(values.newMsgText);
  };
  let dialogsElements;
  let messagesElements;
  if (!props.dialogsElem.dialogs === undefined && props.messagesElem.messages) {
    dialogsElements = props.dialogsElem.dialogs.map((d: any, key: any) => (
      <Dialog name={d.name} id={d.id} key={key} />
    ));

    messagesElements = props.messagesElem.messages.map((m: any, key: any) => (
      <Messages text={m.text} key={key} />
    ));
  }

  return (
    <div className={DialogsStyles.dialogs}>
      <h1 className={DialogsStyles.title}>Dialogs</h1>
      <div className={DialogsStyles.blockWrapper}>
        <div className={DialogsStyles.dialogBlock}>{dialogsElements}</div>
        <div className={DialogsStyles.messageBlock}>
          {messagesElements}
          <MessageReduxForm onSubmit={addNewMessage} />
        </div>
      </div>
    </div>
  );
};

export default Dialogs;
