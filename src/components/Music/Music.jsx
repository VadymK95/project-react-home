import React from 'react';
import MusicStyles from './Music.module.css';

const Music = () => {
  return (
    <div className={MusicStyles.music}>
      <h1 className={MusicStyles.title}>Music</h1>
    </div>
  );
};

export default Music;
