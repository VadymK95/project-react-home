import React from 'react';
import NewsStyles from './News.module.css';

const News = () => {
  return (
    <div className={NewsStyles.news}>
      <h1 className={NewsStyles.title}>News</h1>
    </div>
  );
};

export default News;
