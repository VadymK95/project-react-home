import { FormAction } from 'redux-form';
import { BaseThunkType, InferActionsTypes } from './reduxStore';
import {
  ChatAPI,
  ChatMessageAPIType,
  StatusType,
} from '../components/api/chat-api';
import { Dispatch } from 'redux';
import { v1 } from 'uuid';

type ChatMessageType = ChatMessageAPIType & { id?: string };

const initialState = {
  messages: [] as ChatMessageType[],
  status: 'pending' as StatusType,
};
export type InitialStateType = typeof initialState;
type ActionsTypes = InferActionsTypes<typeof actions>;
type ThunkType = BaseThunkType<ActionsTypes | FormAction>;

const chatReducer = (
  state = initialState,
  action: ActionsTypes | any
): InitialStateType => {
  switch (action.type) {
    case 'MESSAGES_RECEIVED':
      return {
        ...state,
        messages: [
          ...state.messages,
          ...action.payload.messages.map((m: ChatMessageAPIType) => ({
            ...m,
            id: v1(),
          })),
        ].filter((m, index, array) => index >= array.length - 100),
      };
    case 'STATUS_CHANGED':
      return {
        ...state,
        messages: action.payload.status,
      };
    default:
      return state;
  }
};

export const actions = {
  messagesReceived: (messages: ChatMessageType[]) =>
    ({
      type: 'MESSAGES_RECEIVED',
      payload: { messages },
    } as const),
  statusChanged: (status: StatusType) =>
    ({
      type: 'STATUS_CHANGED',
      payload: { status },
    } as const),
};

let _newMessageHandler: ((messages: ChatMessageType[]) => void) | null = null;
const newMessageHandlerCreator = (dispatch: Dispatch) => {
  if (_newMessageHandler === null) {
    _newMessageHandler = messages => {
      dispatch(actions.messagesReceived(messages));
    };
  }
  return _newMessageHandler;
};

let _statusChangedHandler: ((status: StatusType) => void) | null = null;
const statusChangedHandlerCreator = (dispatch: Dispatch) => {
  if (_statusChangedHandler === null) {
    _statusChangedHandler = status => {
      dispatch(actions.statusChanged(status));
    };
  }
  return _statusChangedHandler;
};

export const startMessagesListening = (): ThunkType => async dispatch => {
  ChatAPI.start();
  ChatAPI.subscribe('message-received', newMessageHandlerCreator(dispatch));
  ChatAPI.subscribe('status-changed', statusChangedHandlerCreator(dispatch));
};

export const stopMessagesListening = (): ThunkType => async dispatch => {
  ChatAPI.unsubscribe('message-received', newMessageHandlerCreator(dispatch));
  ChatAPI.unsubscribe('status-changed', statusChangedHandlerCreator(dispatch));
  ChatAPI.stop();
};

export const sendMessage =
  (message: string): ThunkType =>
  async dispatch => {
    ChatAPI.sendMessage(message);
  };

export default chatReducer;
