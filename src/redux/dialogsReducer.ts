import { InferActionsTypes } from './reduxStore';

type DialogType = {
  id: number;
  name: string;
};

type TextType = {
  id: number;
  text: string;
};

let initialState = {
  dialogs: [
    { id: 1, name: 'Vadym' },
    { id: 2, name: 'Iryna' },
    { id: 3, name: 'Vitalyk' },
    { id: 4, name: 'Alexander' },
    { id: 5, name: 'Xaqani' },
  ] as Array<DialogType>,
  messages: [
    { id: 1, text: 'Hello World!' },
    { id: 2, text: 'How are you?' },
    { id: 3, text: 'Thank you!' },
    { id: 4, text: 'Whats your job?' },
    { id: 5, text: 'Thank you!' },
  ] as Array<TextType>,
};

export type InitialStateType = typeof initialState;
type ActionsType = InferActionsTypes<typeof actions>;

const dialogsReducer = (
  state = initialState,
  action: ActionsType
): InitialStateType => {
  switch (action.type) {
    case 'SN/DIALOGS/ADD-MSG':
      let newMsg = {
        id: 6,
        text: action.newMsgText,
      };
      return {
        ...state,
        messages: [...state.messages, newMsg],
      };
    default:
      return state;
  }
};

export const actions = {
  addMsg: (newMsgText: string) =>
    ({ type: 'SN/DIALOGS/ADD-MSG', newMsgText } as const),
};

export default dialogsReducer;
