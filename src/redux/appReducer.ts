import { getAuthUserData } from './authReducer';
import { InferActionsTypes } from './reduxStore';

let initialState = {
  initialized: false,
  globalError: null,
};

export type InitialStateType = typeof initialState;

type ActionsType = InferActionsTypes<typeof actions>;

export const appReducer = (
  state = initialState,
  action: ActionsType
): InitialStateType => {
  switch (action.type as string) {
    case 'SN/APP/INITIALIZED-SUCCESS':
      return {
        ...state,
        initialized: true,
      };
    default:
      return state;
  }
};

export const actions = {
  initializedSuccess: () => ({ type: typeof 'SN/APP/INITIALIZED-SUCCESS' }),
};

export const initializeApp = () => (dispatch: any) => {
  let promiseAuth = dispatch(getAuthUserData());
  Promise.all([promiseAuth]).then(() => {
    dispatch(actions.initializedSuccess());
  });
};

export default appReducer;
