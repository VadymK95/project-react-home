import { ResponseType, ResultCodesEnum } from '../components/api/api';
import { usersAPI } from '../components/api/usersAPI';
import { actions, follow, unfollow } from './usersReducer';

jest.mock('../components/api/usersAPI');

const usersAPIMock = usersAPI as jest.Mocked<typeof usersAPI>;
const result: ResponseType = {
  resultCode: ResultCodesEnum.Success,
  data: {},
  messages: [],
};
// @ts-ignore
usersAPIMock.follow.mockReturnValue(Promise.resolve(result));

test('follow thunk', async () => {
  const thunk = follow(1);
  const dispatchMock = jest.fn();
  const getStateMock = jest.fn();

  // @ts-ignore
  await thunk(dispatchMock, getStateMock);

  expect(dispatchMock).toBeCalledTimes(3);
  expect(dispatchMock).toHaveBeenNthCalledWith(
    1,
    actions.toggleIsfollowing(true, 1)
  );
  expect(dispatchMock).toHaveBeenNthCalledWith(2, {
    isFetching: false,
    type: 'TOGGLE_IS_FOLLOWING_PROGRESS',
    userId: 1,
  });
  expect(dispatchMock).toHaveBeenNthCalledWith(
    3,
    actions.toggleIsfollowing(false, 1)
  );
});

test('unfollow thunk', async () => {
  const thunk = unfollow(1);
  const dispatchMock = jest.fn();
  const getStateMock = jest.fn();

  // @ts-ignore
  await thunk(dispatchMock, getStateMock);

  expect(dispatchMock).toBeCalledTimes(3);
  expect(dispatchMock).toHaveBeenNthCalledWith(
    1,
    actions.toggleIsfollowing(true, 1)
  );
  expect(dispatchMock).toHaveBeenNthCalledWith(2, actions.toggleIsfollowing(false, 1));
  expect(dispatchMock).toHaveBeenNthCalledWith(
    3,
    actions.toggleIsfollowing(false, 1)
  );
});
