import {
  ResultCodesEnum,
  ResultCodesWithCaptchaEnum,
} from '../components/api/api';
import { authAPI } from '../components/api/authAPI';
import { securityAPI } from '../components/api/securityAPI';
import { FormAction, stopSubmit } from 'redux-form';
import { BaseThunkType, InferActionsTypes } from './reduxStore';

const initialState = {
  userId: null as number | null,
  email: null as string | null,
  login: null as string | null,
  isAuth: false,
  isFetching: false,
  captchaUrl: null as string | null,
};
export type InitialStateType = typeof initialState;
type ActionsTypes = InferActionsTypes<typeof actions>;
type ThunkType = BaseThunkType<ActionsTypes | FormAction>;

const authReducer = (
  state = initialState,
  action: ActionsTypes | any
): InitialStateType => {
  switch (action.type) {
    case 'MYAPP/AUTH/SET-USER-DATA':
    case 'GET-CAPTCHA-URL-SUCCESS':
      return {
        ...state,
        ...action.payload,
      };
    case 'TOGGLE-IS-FETCHING':
      return {
        ...state,
        isFetching: action.isFetching,
      };
    default:
      return state;
  }
};

export const actions = {
  setAuthUserData: (
    userId: number | null,
    email: string | null,
    login: string | null,
    isAuth: boolean | null
  ) =>
    ({
      type: 'MYAPP/AUTH/SET-USER-DATA',
      payload: { userId, email, login, isAuth },
    } as const),
  getCaptchaUrlSuccess: (captchaUrl: string) =>
    ({
      type: 'GET-CAPTCHA-URL-SUCCESS',
      payload: { captchaUrl },
    } as const),
  toggleIsFetching: (isFetching: boolean) => ({
    type: 'TOGGLE-IS-FETCHING',
    isFetching,
  }),
};

export const getAuthUserData = (): ThunkType => async dispatch => {
  const meData = await authAPI.me();
  if (meData.resultCode === ResultCodesEnum.Success) {
    let { id, email, login } = meData.data;
    dispatch(actions.setAuthUserData(id, email, login, true));
    dispatch(actions.toggleIsFetching(false));
  }
};

export const login =
  (
    email: string,
    password: string,
    rememberMe: boolean,
    captcha: string
  ): ThunkType =>
  async dispatch => {
    const data = await authAPI.login(email, password, rememberMe, captcha);
    if (data.resultCode === ResultCodesEnum.Success) {
      dispatch(getAuthUserData());
    } else {
      if (data.resultCode === ResultCodesWithCaptchaEnum.CaptchaIsValid) {
        dispatch(getCaptchaUrl());
      }
      let message = data.messages.length > 0 ? data.messages[0] : 'Some error';
      dispatch(stopSubmit('login', { _error: message }));
    }
  };

export const getCaptchaUrl = () => async (dispatch: any) => {
  const response = await securityAPI.getCaptchaUrl();
  const captchaUrl = response.url;
  dispatch(actions.getCaptchaUrlSuccess(captchaUrl));
};

export const logout = (): ThunkType => async (dispatch: any) => {
  const data = await authAPI.logout();
  if (data.resultCode === ResultCodesEnum.Success) {
    dispatch(actions.setAuthUserData(null, null, null, false));
  }
};

export default authReducer;
