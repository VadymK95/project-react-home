import { PhotoType, PostsType, ProfileType } from './../types/types';
import { profileAPI } from '../components/api/profileAPI';
import { FormAction, stopSubmit } from 'redux-form';
import { BaseThunkType, InferActionsTypes } from './reduxStore';

const initialState = {
  posts: [
    {
      id: 1,
      textPost:
        'The sea, the world ocean or simply the ocean is the connected body of salty water...',
      likes: 17,
    },
    { id: 2, textPost: 'or simply the ocean is the', likes: 3 },
    { id: 3, textPost: 'the world ocean or simply the ocean is the', likes: 9 },
    { id: 4, textPost: 'connected body of salty water', likes: 33 },
    { id: 5, textPost: 'The sea, the world ocean or', likes: 2 },
  ] as Array<PostsType>,
  profile: null as ProfileType | null,
  status: '',
};
type ActionsType = InferActionsTypes<typeof actions>;
type ThunkType = BaseThunkType<ActionsType | FormAction>;

export const actions = {
  addPostActionCreator: (newPostText: string) =>
    ({ type: 'ADD-POST', newPostText } as const),
  setUserProfile: (profile: ProfileType) =>
    ({ type: 'SET-USER-PROFILE', profile } as const),
  setStatus: (status: string) =>
    ({
      type: 'SET-STATUS',
      status,
    } as const),
  savePhotoSuccess: (photos: PhotoType) =>
    ({ type: 'SAVE-PHOTOS', photos } as const),
};

export type initialStateType = typeof initialState;

const profileReducer = (
  state = initialState,
  action: ActionsType
): initialStateType => {
  switch (action.type) {
    case 'ADD-POST': {
      let newPost = {
        id: 5,
        textPost: action.newPostText,
        likes: 0,
      };
      return {
        ...state,
        posts: [...state.posts, newPost],
      };
    }
    case 'SET-USER-PROFILE':
      return {
        ...state,
        profile: action.profile,
      };
    case 'SET-STATUS':
      return {
        ...state,
        status: action.status,
      };
    case 'SAVE-PHOTOS':
      return {
        ...state,
        profile: {
          ...state.profile,
          photos: action.photos,
        } as ProfileType,
      };
    default:
      return state;
  }
};

export const getUserProfile =
  (userId: number): ThunkType =>
  async dispatch => {
    const response = await profileAPI.getProfile(userId);
    dispatch(actions.setUserProfile(response));
  };

export const getStatus =
  (userId: number): ThunkType =>
  async dispatch => {
    const response = await profileAPI.getStatus(userId);
    dispatch(actions.setStatus(response));
  };

export const updateStatus =
  (status: string): ThunkType =>
  async dispatch => {
    try {
      const response = await profileAPI.updateStatus(status);
      if (response.resultCode === 0) {
        dispatch(actions.setStatus(status));
      }
    } catch (err) {}
  };

export const savePhoto =
  (file: File): ThunkType =>
  async dispatch => {
    const response = await profileAPI.savePhoto(file);
    if (response.data.resultCode === 0) {
      dispatch(actions.savePhotoSuccess(response.data.data));
    }
  };

export const saveProfile =
  (profile: ProfileType): ThunkType =>
  async (dispatch, getState) => {
    const userId = getState().auth.userId;
    const response = await profileAPI.saveProfile(profile);
    if (response.data.resultCode === 0) {
      if (userId != null) {
        dispatch(getUserProfile(userId));
      } else {
        throw new Error("userId can't be null");
      }
    } else {
      dispatch(
        stopSubmit('edit-profile', { _error: response.data.messages[0] })
      );
      return Promise.reject(response.data.messages[0]);
    }
  };

export default profileReducer;
