import { createSelector } from 'reselect';
import { AppStateType } from './reduxStore';

const getUserSelector = (state: AppStateType) => {
  return state.usersPage.users;
};

export const getUser = createSelector(getUserSelector, users => {
  return users;
});

export const getPageSize = (state: AppStateType) => {
  return state.usersPage.pageSize;
};

export const getTotalUsersCount = (state: AppStateType) => {
  return state.usersPage.totalUsersCount;
};

export const getCurrentPage = (state: AppStateType) => {
  return state.usersPage.currentPage;
};

export const getIsFetching = (state: AppStateType) => {
  return state.usersPage.isFetching;
};

export const getToggleIsfollowing = (state: any) => {
  return state.usersPage.toggleIsfollowing;
};

export const getFollowingInProgress = (state: AppStateType) => {
  return state.usersPage.followingInProgress;
};

export const getUsersFilter = (state: AppStateType) => {
  return state.usersPage.filter;
};